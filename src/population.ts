import { Dot } from "./dot";
import { Goal } from "./goal";
import { Obstacle } from "./obstacle";
import { Vector3 } from "math3d";

export class Population {
    private dots: Dot[] = [];
    private obstacles: Obstacle[] = [];
    private goal: Goal = new Goal();
    private generation: number = 1;
    private maxSteps: number = 500;
    private totalFitnessOfGeneration = 0;

    constructor(private nrOfDots: number, private maxWidth: number, private maxHeight: number) {
        for (let i = 0; i < nrOfDots; i++) {
            this.dots.push(new Dot(this.maxSteps));
        }
        this.obstacles.push(new Obstacle(new Vector3(0, 300 , 0), 400, 10));
        this.obstacles.push(new Obstacle(new Vector3(300, 400, 0), 500, 10));
    }

    // Getters

    public getDots(): Dot[] {
        return this.dots;
    }

    public getGeneration(): number {
        return this.generation;
    }

    private getNrOfDeadDots(): number {
        return this.dots.filter(d => !d.isAlive()).length;
    }

    public getNumberOfFinishers(): number {
        return this.dots.filter(d => d.isFinished()).length;
    }

    public isAllDeadOrFinished(): boolean {
        return this.getNumberOfFinishers() + this.getNrOfDeadDots() == this.dots.length;
    }

    public getConsecutiveWins(dotIndex: number): number {
        return this.dots[dotIndex].getConsecutiveWins();
    }

    public getNumberOfSteps(dotIndex: number): number {
        return this.dots[dotIndex].getSteps();
    }

    // Public methods

    /**
     * Moves and draws the scene, including dots and goal, one frame.
     * @param context The canvas to draw the scene on.
     */
    public doOneFrame(context: CanvasRenderingContext2D): void {
        this.goal.draw(context);

        for(let obstacle of this.obstacles) {
            obstacle.draw(context);
        }

        for (let i = 1; i < this.dots.length; i++) {
            let dot = this.dots[i];
            this.moveDot(dot);

            dot.draw(context);
        }

        //Draw best dot last so that it is on top
        let bestDot = this.dots[0];
        this.moveDot(bestDot);
        bestDot.draw(context);
    }

    /**
     * Calculate the fitness of all dots in the population.
     */
    public calculateFitness(): void {
        for (let dot of this.dots) {
            dot.calculateFitness(this.goal, this.obstacles);
        }
    }

    /**
     * Gets the fitness and index of the best dot this generation.
     */
    public getBestFitness(): [number, number] {
        let maxFitness: number = 0;
        let index: number = -1;
        this.dots.forEach((dot, i) => {
            if (dot.getFitness() > maxFitness) {
                maxFitness = dot.getFitness();
                index = i;
            }
        });
        return [maxFitness, index];
    }

    /**
     * Create a new generation.
     * The best dot lives on and the rest of the new population
     * is created from randomly selected (weighted on fitness) parent.
     */
    public naturalSelection(): void {
        let newDots: Dot[] = [];
        let bestTuple = this.getBestFitness();
        let bestIndex = bestTuple["1"];
        let bestDot = this.dots[bestIndex];
        if (bestDot.isFinished()) {
            this.maxSteps = bestDot.getSteps();
        }

        // The best dot gets to live in next gen
        newDots[0] = bestDot.duplicateDot(this.maxSteps);
        newDots[0].setBest();
        newDots[0].setConsecutiveWins(bestDot.getConsecutiveWins() + 1);

        this.calculateTotalFitness();
        for (let i = 1; i < this.dots.length; i++) {
            let parent: Dot = this.getRandomDot();

            newDots[i] = parent.duplicateDot(this.maxSteps);
        }
        this.generation++;
        this.dots = newDots;
    }

    /**
     * Mutates the population of Dots.
     * The best dot in the last generation is not mutated.
     */
    public mutateNewGeneration(): void {
        for (let dot of this.dots) {
            if (!dot.isBest()) {
                dot.mutate();
            }
        }
    }

    // Private methods

    private moveDot(dot: Dot): void {
        if (this.isOutsideBoundries(dot)) {
            dot.setDead();
        }
        if (this.hasReachedGoal(dot)) {
            dot.setFinished();
        }
        for(let obstacle of this.obstacles) {
            if(dot.isInsideObstacle(obstacle)) {
                dot.setDead();
            }
        }

        dot.move();
    }

    private isOutsideBoundries(dot: Dot): boolean {
        return dot.getPos().y > this.maxHeight - dot.getRadius() ||
            dot.getPos().x > this.maxWidth - dot.getRadius() ||
            dot.getPos().x < dot.getRadius() ||
            dot.getPos().y < dot.getRadius();
    }

    private hasReachedGoal(dot: Dot): boolean {
        let distanceToGoal = dot.getPos().distanceTo(this.goal.getPos());
        return distanceToGoal < dot.getRadius() + this.goal.getRadius();
    }

    private calculateTotalFitness(): void {
        let totalFitness = 0;
        for (let dot of this.dots) {
            totalFitness += dot.getFitness();
        }
        this.totalFitnessOfGeneration = totalFitness;
    }

    private getRandomDot(): Dot {
        let random = Math.random() * this.totalFitnessOfGeneration;
        let tempRand = 0;
        for (let dot of this.dots) {
            tempRand += dot.getFitness();
            if (tempRand > random) {
                return dot;
            }
        }
        console.error("Should NEVER happen.");
        return new Dot(0);
    }
}