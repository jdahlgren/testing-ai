import { Vector3 } from "math3d";

export class Brain {
    private steps: Vector3[] = [];
    private currentStep: number = 0;

    constructor(nrOfSteps: number) {
        for (let i = 0; i < nrOfSteps; i++) {
            this.steps.push(this.getNewRandomDirection());
        }
    }

    // Getters 

    public getStep(): number {
        return this.currentStep;
    }

    // Public methods

    /**
     * Gets the next step in this brain.
     * If all steps this brain can handle is taken, undefined is returned.
     */
    public getNextStep(): Vector3 | undefined {
        if (this.currentStep < this.steps.length) {
            let nextStep = this.steps[this.currentStep];
            this.currentStep++;
            return nextStep;
        }
        else {
            return undefined;
        }
    }

    /**
     * Clones this brain to a new brain.
     * @param nrOfSteps The number of steps this brain can handle
     */
    public clone(nrOfSteps: number): Brain {
        let newBrain = new Brain(nrOfSteps);
        for (let i = 0; i < nrOfSteps; i++) {
            newBrain.steps[i] = this.steps[i];
        }
        return newBrain;
    }

    /**
     * Mutates this brain.
     * Each direction has a chance of beeing mutated into a new random direction.
     */
    public mutate(): void {
        let mutateRate = 0.01;
        for (let i = 0; i < this.steps.length; i++) {
            let random = Math.random();
            if (mutateRate > random) {
                this.steps[i] = this.getNewRandomDirection();
            }
        }
    }

    // Private methods

    private getNewRandomDirection(): Vector3 {
        return new Vector3(this.getRandomNumber(1, -1), this.getRandomNumber(1, -1), 0)
    }

    private getRandomNumber(min: number, max: number): number {
        return Math.random() * (max - min) + min;
    }
}