import { Vector3 } from "math3d";

export class Goal {
    pos: Vector3 = new Vector3(300, 75, 0);
    color: string = "rgb(0, 0, 255)";
    radius: number = 10;

    // Getters

    public getPos(): Vector3 {
        return this.pos;
    }

    public getRadius(): number {
        return this.radius;
    }

    // Public methods

    /**
     * Draws this goal to the screen.
     * @param context The canvas to draw on.
     */
    public draw(context: CanvasRenderingContext2D): void {
        context.fillStyle = this.color;
        context.beginPath();
        context.arc(this.pos.x, this.pos.y, this.radius, 0, 2 * Math.PI);
        context.fill();
    }
}