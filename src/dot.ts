import { Vector3 } from "math3d";
import { Brain } from "./brain";
import { Goal } from "./goal";
import { Obstacle } from "./obstacle";

export class Dot {
    private vel: Vector3 = new Vector3();
    private pos: Vector3 = new Vector3(400, 550, 0);
    private acc: Vector3 = new Vector3();
    private color: string = "rgb(255, 0, 0)";
    private alive: boolean = true;
    private finished: boolean = false;
    private radius: number = 4;
    private fitness: number = 0;
    private best: boolean = false;
    private consecutiveWins: number = 1;
    private obstaclesPassed = 0;
    private brain: Brain;

    constructor(nrOfSteps: number) {
        this.brain = new Brain(nrOfSteps);
    }

    // Getters

    public getPos(): Vector3 {
        return this.pos;
    }

    public getRadius(): number {
        return this.radius;
    }

    public getFitness(): number {
        return this.fitness;
    }

    public getSteps(): number {
        return this.brain.getStep();
    }

    public getConsecutiveWins(): number {
        return this.consecutiveWins;
    }

    public isFinished(): boolean {
        return this.finished;
    }

    public isAlive(): boolean {
        return this.alive;
    }

    public isBest(): boolean {
        return this.best;
    }

    public getObstaclesPassed(): number {
        return this.obstaclesPassed;
    }

    // Setters

    public setConsecutiveWins(newWins: number): void {
        this.consecutiveWins = newWins;
    }

    public setDead(): void {
        this.alive = false;
        if (!this.isBest()) {
            this.color = "rgb(0,0,0)";
        }
    }

    public setFinished(): void {
        this.finished = true;
        if (!this.isBest()) {
            this.color = "rgb(0, 255, 0)";
        }
    }

    public setBest(): void {
        this.best = true;
        this.color = "rgb(255, 255, 0)";
    }

    // Public methods

    /**
     * How good was this dot at reaching the goal?
     * Finishers have a better score than non finishers.
     * @param goal The goal to measure against.
     */
    public calculateFitness(goal: Goal, obstacles: Obstacle[]): void {
        let dist = this.getPos().distanceTo(goal.getPos());
        let obstacleFitness = this.calcObstaclesPassed(obstacles) / 100;

        this.fitness = (1 / dist) + obstacleFitness;
        if (this.isFinished()) {
            // Finishers have better fitness!
            // Don't count closeness, only steps taken to goal.
            this.fitness = 100 / this.brain.getStep();
        }
    }

    /**
     * Create a new Dot with the same brain as this dot.
     * @param maxSteps The number of steps the new dot can handle.
     */
    public duplicateDot(maxSteps: number): Dot {
        let newDot = new Dot(maxSteps);
        newDot.brain = this.brain.clone(maxSteps);
        return newDot;
    }

    /**
     * Mutates the brain of this dot.
     */
    public mutate(): void {
        this.brain.mutate();
    }

    /**
     * Calculate the number of obstacles that this dot has passed.
     * A dot has passed an obstacle if its y-pos is less then the y-pos of the obstacle
     * @param obstacles the list of obstacles to measure against.
     */
    public calcObstaclesPassed(obstacles: Obstacle[]): number {
        this.obstaclesPassed = 0;
        for(let obstacle of obstacles) {
            if(this.pos.y < obstacle.getPos().y) {
                this.obstaclesPassed++;
            }
        }
        return this.obstaclesPassed;
    }

    /**
     * Moves this dot one step if it is alive or hasn't finished.
     * Kills the dot if it has taken all steps its brain can handle.
     */
    public move(): void {
        if (this.isAlive() && !this.isFinished()) {
            let acc = this.brain.getNextStep();
            if (acc === undefined) {
                this.setDead();
                return;
            }
            this.acc = acc;
            this.vel = this.vel.add(this.acc);
            this.limitVelTo(5);
            this.pos = this.pos.add(this.vel);
        }
    }

    /**
     * Draws this dot to the screen.
     * @param context The canvas to draw on.
     */
    public draw(context: CanvasRenderingContext2D): void {
        context.fillStyle = this.color;
        context.beginPath();
        if (this.isBest()) {
            context.arc(this.pos.x, this.pos.y, this.radius * 2, 0, 2 * Math.PI);
        }
        else {
            context.arc(this.pos.x, this.pos.y, this.radius, 0, 2 * Math.PI);
        }

        context.fill();
    }

    /**
     * 
     * @param dot 
     */
    public isInsideObstacle(obstacle: Obstacle): boolean {
        return this.getPos().x > obstacle.getPos().x &&
            this.getPos().x < obstacle.getPos().x + obstacle.getWidth() &&
            this.getPos().y > obstacle.getPos().y &&
            this.getPos().y < obstacle.getPos().y + obstacle.getHeight();
    }

    // Private methods

    private limitVelTo(maxVel: number): void {
        if (this.vel.x > maxVel) {
            this.vel = new Vector3(maxVel, this.vel.y, 0);
        }
        else if (this.vel.x < -maxVel) {
            this.vel = new Vector3(-maxVel, this.vel.y, 0);
        }
        if (this.vel.y > maxVel) {
            this.vel = new Vector3(this.vel.x, maxVel, 0);
        }
        else if (this.vel.y < -maxVel) {
            this.vel = new Vector3(this.vel.x, -maxVel, 0);
        }
    }
}
