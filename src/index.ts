import { Population } from "./population";

var canvas: HTMLCanvasElement;
var population: Population;
var context: CanvasRenderingContext2D;
var maxHeight: number;
var maxWidth: number;
var stats: HTMLElement;
var animationId: number;

setup();
startSimulation();

function setup(): void {
    canvas = document.createElement("canvas");
    canvas.height = 600;
    canvas.width = 800;

    context = canvas.getContext("2d")!;
    maxHeight = canvas.height;
    maxWidth = canvas.width;

    let onlyDiv = document.getElementById("myDiv")!;
    onlyDiv.insertAdjacentElement("afterend", canvas);

    let restartButton = document.getElementById("restartButton")!;
    restartButton.addEventListener("click", () => restartSimulation());

    let stopButton = document.getElementById("stopButton")!;
    stopButton.addEventListener("click", () => stopSimulaton());

    stats = document.getElementById("stats")!;
}

function startSimulation(): void {
    localStorage.clear();
    population = new Population(300, maxWidth, maxHeight);
    stats.innerText = "Generation: " + population.getGeneration();

    animationId = requestAnimationFrame(() => run());
}

function stopSimulaton(): void {
    window.cancelAnimationFrame(animationId);
}

function logGenerationToLocalStorage() {
    let i = 0;
    let temp: {}[] = [];
    for (let dot of population.getDots()) {
        let obj = {
            "dotIndex": i,
            "fitness": dot.getFitness(),
            "steps": dot.getSteps(),
            "pos": dot.getPos().x + ", " + dot.getPos().y,
            "obstacles": dot.getObstaclesPassed()
        };
        temp.push(obj);
        i++;
    }
   
    localStorage.setItem(population.getGeneration().toString(), JSON.stringify(temp));
}

function restartSimulation(): void {
    stopSimulaton();
    startSimulation();
}

function startNewGeneration(): void {
    population.calculateFitness();

    printStats();
    // TODO localstorage gets full....
    //logGenerationToLocalStorage();

    population.naturalSelection();
    population.mutateNewGeneration();

    animationId = requestAnimationFrame(() => run());
}

function run(): void {
    if (population.isAllDeadOrFinished()) {

        startNewGeneration();
    }
    else {
        context.clearRect(0, 0, maxWidth, maxHeight);
        population.doOneFrame(context);

        animationId = requestAnimationFrame(() => run());
    }
}

function printStats(): void {
    let bestFitness = population.getBestFitness();
    let bestDotIndex = bestFitness["1"];
    let gen = population.getGeneration() + 1;
    stats.innerText = "Generation: " + gen
        + " ### Stats from last gen: "
        + " -- Finishers: " + population.getNumberOfFinishers()
        + " -- Best dot: " + bestDotIndex
        + " -- Consecutive wins: " + population.getConsecutiveWins(bestDotIndex)
        + " -- Steps to win: " + population.getNumberOfSteps(bestDotIndex);

}
