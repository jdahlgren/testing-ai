import { Vector3 } from "math3d";
import { Dot } from "./dot";

export class Obstacle {
    private color: string = "rgb(255, 0, 255)";

    constructor(private pos: Vector3, private width: number, private height: number) {

    }

    // Getters
    public getPos(): Vector3 {
        return this.pos;
    }

    public getHeight(): number {
        return this.height;
    }

    public getWidth(): number {
        return this.width;
    }

    // Public methods
    public draw(context: CanvasRenderingContext2D): void {
        context.fillStyle = this.color;
        context.fillRect(this.pos.x, this.pos.y, this.width, this.height);
    }
}